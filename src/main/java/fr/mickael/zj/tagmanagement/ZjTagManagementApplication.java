package fr.mickael.zj.tagmanagement;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ZjTagManagementApplication {

    public static void main(String[] args) {
        SpringApplication.run(ZjTagManagementApplication.class, args);
    }

}
